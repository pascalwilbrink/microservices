package nl.wilbrink.actors.controller;

import nl.wilbrink.actors.dto.ActorDTO;
import nl.wilbrink.actors.service.ActorsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/actors")
public class ActorsController {

    private final ActorsService actorsService;

    @Autowired
    public ActorsController(ActorsService actorsService) {
        this.actorsService = actorsService;
    }

    @RequestMapping
    public ResponseEntity getActors() {
        List<ActorDTO> actors = actorsService.getActors();

        return ResponseEntity.ok(actors);
    }

    @RequestMapping("/{id}")
    public ResponseEntity getActor(@PathVariable("id") Long id) {
        ActorDTO actor = actorsService.getActor(id);

        return ResponseEntity.ok(actor);
    }

    @RequestMapping(method= RequestMethod.POST)
    public ResponseEntity createActor(@RequestBody ActorDTO actor) {
        actor = actorsService.createActor(actor);

        return ResponseEntity.ok(actor);
    }

    @RequestMapping(value="/{id}", method= RequestMethod.PUT)
    public ResponseEntity updateActor(@PathVariable("id") Long id, @RequestBody ActorDTO actor) {
        actor.setId(id);

        actor = actorsService.updateActor(actor);

        return ResponseEntity.ok(actor);
    }

    @RequestMapping(value="/{id}", method= RequestMethod.DELETE)
    public ResponseEntity deleteActor(@PathVariable("id") Long id) {
        actorsService.deleteActor(id);

        return ResponseEntity.ok().build();
    }
}
