package nl.wilbrink.actors.mapper;

import nl.wilbrink.actors.dto.ActorDTO;
import nl.wilbrink.actors.entity.Actor;
import org.mapstruct.Mapper;

@Mapper( componentModel = "spring" )
public abstract class ActorMapper {

    public abstract ActorDTO toDTO(Actor entity);

    public abstract Actor toEntity(ActorDTO dto);

}
