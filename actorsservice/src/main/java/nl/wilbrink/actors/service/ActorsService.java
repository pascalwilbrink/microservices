package nl.wilbrink.actors.service;

import nl.wilbrink.actors.dto.ActorDTO;
import nl.wilbrink.actors.entity.Actor;
import nl.wilbrink.actors.mapper.ActorMapper;
import nl.wilbrink.actors.repository.ActorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ActorsService {

    private final ActorRepository actorRepository;

    private final ActorMapper actorMapper;

    @Autowired
    public ActorsService(ActorRepository actorRepository, ActorMapper actorMapper) {
        this.actorRepository = actorRepository;
        this.actorMapper = actorMapper;
    }

    public List<ActorDTO> getActors() {
        List<ActorDTO> actors = new ArrayList<>();

        actorRepository.findAll()
                .forEach(actor -> {
                    actors.add(actorMapper.toDTO(actor));
                });

        return actors;
    }

    public ActorDTO getActor(Long id) {
        Actor entity = actorRepository.findOne(id);

        return actorMapper.toDTO(entity);
    }

    public ActorDTO createActor(ActorDTO actor) {
        Actor entity = actorMapper.toEntity(actor);

        entity = actorRepository.save(entity);

        return actorMapper.toDTO(entity);
    }

    public ActorDTO updateActor(ActorDTO actor) {
        Actor entity = actorMapper.toEntity(actor);

        entity = actorRepository.save(entity);

        return actorMapper.toDTO(entity);
    }

    public void deleteActor(Long id) {
        actorRepository.delete(id);
    }

}
