-- Create Actors table
create table actors (
    id int NOT NULL PRIMARY KEY,
    name varchar(255) NOT NULL,
    photo varchar(500)
);
