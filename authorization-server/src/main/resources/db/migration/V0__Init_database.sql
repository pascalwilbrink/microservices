-- Create Users table
create table users (
    id int NOT NULL PRIMARY KEY,
    username varchar(255) NOT NULL UNIQUE,
    password varchar(500),
    enabled boolean
);