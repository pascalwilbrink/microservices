-- Create users
-- Password = pass
insert into users(id,username,password,enabled) values(1,'user1','$2a$10$VWi5qMMTioSgp.o6pSnARu6v2qbuI846rm8e2mT2Pgbn7jVJ9VHXi',true);
insert into users(id,username,password,enabled) values(2,'user2','$2a$10$VWi5qMMTioSgp.o6pSnARu6v2qbuI846rm8e2mT2Pgbn7jVJ9VHXi',true);
insert into users(id,username,password,enabled) values(3,'user3','$2a$10$VWi5qMMTioSgp.o6pSnARu6v2qbuI846rm8e2mT2Pgbn7jVJ9VHXi',true);
insert into users(id,username,password,enabled) values(4,'user4','$2a$10$VWi5qMMTioSgp.o6pSnARu6v2qbuI846rm8e2mT2Pgbn7jVJ9VHXi',false);
