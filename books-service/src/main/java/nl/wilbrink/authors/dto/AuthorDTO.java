package nl.wilbrink.authors.dto;

import nl.wilbrink.common.dto.BaseDTO;

import java.io.Serializable;

public class AuthorDTO extends BaseDTO implements Serializable {

    private String name;

    private String summary;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getSummary() {
        return summary;
    }

}
