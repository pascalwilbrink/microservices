package nl.wilbrink.authors.entity;

import nl.wilbrink.common.entity.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name="authors")
public class Author extends BaseEntity implements Serializable {

    @Column
    private String name;

    @Column
    private String summary;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getSummary() {
        return summary;
    }

}
