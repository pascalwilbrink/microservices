package nl.wilbrink.authors.mapper;

import nl.wilbrink.authors.dto.AuthorDTO;
import nl.wilbrink.authors.entity.Author;
import org.mapstruct.Mapper;

@Mapper(componentModel="spring")
public abstract class AuthorMapper {

    public abstract AuthorDTO toDTO(Author entity);

    public abstract Author toEntity(AuthorDTO dto);

}
