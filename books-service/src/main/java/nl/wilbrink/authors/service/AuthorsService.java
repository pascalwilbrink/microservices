package nl.wilbrink.authors.service;

import nl.wilbrink.authors.dto.AuthorDTO;
import nl.wilbrink.authors.entity.Author;
import nl.wilbrink.authors.mapper.AuthorMapper;
import nl.wilbrink.authors.repository.AuthorRepository;
import nl.wilbrink.common.exception.WebException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class AuthorsService {

    private final AuthorRepository authorRepository;

    private final AuthorMapper authorMapper;

    @Autowired
    public AuthorsService(AuthorRepository authorRepository, AuthorMapper authorMapper) {
        this.authorRepository = authorRepository;
        this.authorMapper = authorMapper;

        assert(this.authorRepository != null);
        assert(this.authorMapper != null);
    }

    public AuthorDTO getAuthorById(Long id) {
        Author entity = findAuthorEntity(id);

        return authorMapper.toDTO(entity);
    }

    private Author findAuthorEntity(Long id) {
        Author entity = authorRepository.findOne(id);

        if(entity == null) {
            throw new WebException(HttpStatus.BAD_REQUEST, "Author not found");
        }

        return entity;
    }
}
