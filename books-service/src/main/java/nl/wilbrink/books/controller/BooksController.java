package nl.wilbrink.books.controller;

import nl.wilbrink.books.dto.BookDTO;
import nl.wilbrink.books.dto.BookPageDTO;
import nl.wilbrink.books.dto.SearchBookRequest;
import nl.wilbrink.books.service.BooksService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/books")
public class BooksController {

    private final BooksService booksService;

    @Autowired
    public BooksController(BooksService booksService) {
        this.booksService = booksService;

        assert(this.booksService != null);
    }

    @RequestMapping(value="/search",method= RequestMethod.POST)
    public ResponseEntity searchBooks(@RequestBody SearchBookRequest searchRequest) {
        BookPageDTO page = booksService.searchBooks(searchRequest);

        return ResponseEntity.ok(page);
    }

    @RequestMapping(value="/{id}", method=RequestMethod.GET)
    public ResponseEntity getBook(@PathVariable("id") Long id) {
        BookDTO book = booksService.getBook(id);

        return ResponseEntity.ok(book);
    }
}
