package nl.wilbrink.books.dto;

import nl.wilbrink.common.dto.BaseDTO;
import org.springframework.data.domain.Page;

import java.util.List;

public class BookPageDTO {

    private int page;

    private int pageSize;

    private List<BookDTO> elements;

    private long total;

    public BookPageDTO(Page page, List<BookDTO> elements) {
        this.page = page.getNumber();
        this.pageSize = page.getSize();
        this.total = page.getTotalElements();

        this.elements = elements;
    }

    public BookPageDTO(int page, int pageSize, int total, List<BookDTO> elements) {
        this.page = page;
        this.pageSize = pageSize;
        this.total = total;

        this.elements = elements;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPage() {
        return page;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setElements(List<BookDTO> elements) {
        this.elements = elements;
    }

    public List<BookDTO> getElements() {
        return elements;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public long getTotal() {
        return total;
    }
}
