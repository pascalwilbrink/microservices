package nl.wilbrink.books.dto;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import static nl.wilbrink.books.entity.QBook.book;
import org.springframework.util.StringUtils;

public class SearchBookCriteria {

    private String title;

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public Predicate toPredicate() {
        BooleanBuilder builder = new BooleanBuilder();

        if(StringUtils.hasText(title)) {
            builder
                    .and(book.title.containsIgnoreCase(title.trim()));
        }

        return builder;
    }
}
