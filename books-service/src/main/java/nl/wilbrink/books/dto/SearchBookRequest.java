package nl.wilbrink.books.dto;

import nl.wilbrink.common.dto.PageForm;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.util.StringUtils;

public class SearchBookRequest {

    private PageForm pageForm = new PageForm();

    private SearchBookCriteria searchCriteria = new SearchBookCriteria();

    public PageForm getPageForm() {
        return pageForm;
    }

    public void setPageForm(PageForm pageForm) {
        this.pageForm = pageForm;
    }

    public void setSearchCriteria(SearchBookCriteria searchCriteria) {
        this.searchCriteria = searchCriteria;
    }

    public SearchBookCriteria getSearchCriteria() {
        return searchCriteria;
    }

    public Pageable toPageable() {
        Sort sort;

        if(StringUtils.hasText(pageForm.getSortField())) {
            sort = new Sort(
                    new Sort.Order(
                            Sort.Direction.fromStringOrNull(pageForm.getSortDirection() ),
                            pageForm.getSortField()
                    )
            );
        } else {
            sort = new Sort("created");
        }

        int size = pageForm.getSize();

        if(size <= 0 || size > 100) {
            size = 20;
        }

        return new PageRequest(pageForm.getPage() - 1, size, sort);
    }
}

