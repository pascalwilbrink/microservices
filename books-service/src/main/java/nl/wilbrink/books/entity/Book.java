package nl.wilbrink.books.entity;

import nl.wilbrink.authors.entity.Author;
import nl.wilbrink.common.entity.BaseEntity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="books")
public class Book extends BaseEntity implements Serializable {

    @Column
    private String title;

    @Column
    private String summary;

    @ManyToOne
    @JoinColumn(name="author_id")
    private Author author;

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getSummary() {
        return summary;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public Author getAuthor() {
        return author;
    }

}
