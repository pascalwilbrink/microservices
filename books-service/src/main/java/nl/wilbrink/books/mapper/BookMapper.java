package nl.wilbrink.books.mapper;

import nl.wilbrink.books.dto.BookDTO;
import nl.wilbrink.books.entity.Book;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public abstract class BookMapper {

    public abstract BookDTO toDTO(Book entity);

    public abstract Book toEntity(BookDTO dto);

}
