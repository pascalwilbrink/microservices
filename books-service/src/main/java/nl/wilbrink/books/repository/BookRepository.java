package nl.wilbrink.books.repository;

import nl.wilbrink.books.entity.Book;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookRepository extends PagingAndSortingRepository<Book,Long>, QueryDslPredicateExecutor {
}
