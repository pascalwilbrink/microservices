package nl.wilbrink.books.service;

import com.querydsl.core.types.Predicate;
import nl.wilbrink.authors.service.AuthorsService;
import nl.wilbrink.books.dto.BookDTO;
import nl.wilbrink.books.dto.BookPageDTO;
import nl.wilbrink.books.dto.SearchBookRequest;
import nl.wilbrink.books.entity.Book;
import nl.wilbrink.books.mapper.BookMapper;
import nl.wilbrink.books.repository.BookRepository;
import nl.wilbrink.common.exception.WebException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class BooksService {

    private final BookRepository bookRepository;

    private final BookMapper bookMapper;

    private final AuthorsService authorsService;

    @Autowired
    public BooksService(BookRepository bookRepository, BookMapper bookMapper, AuthorsService authorsService) {
        this.bookRepository = bookRepository;
        this.bookMapper = bookMapper;
        this.authorsService = authorsService;

        assert(this.bookRepository != null);
        assert(this.bookMapper != null);
        assert(this.authorsService != null);
    }

    public BookPageDTO searchBooks(SearchBookRequest searchRequest) {
        List<BookDTO> books = new ArrayList<>();

        Predicate predicate = searchRequest.getSearchCriteria().toPredicate();
        Pageable pageable = searchRequest.toPageable();

        Page page = bookRepository.findAll(predicate,pageable);

        page.getContent().forEach(entity-> {
            books.add(bookMapper.toDTO((Book)entity));
        });

        return new BookPageDTO(page,books);
    }

    public BookDTO getBook(Long id) {
        Book entity = findBookEntity(id);

        return bookMapper.toDTO(entity);
    }

    private Book findBookEntity(Long id) {
        Book entity = bookRepository.findOne(id);

        if(entity == null) {
            throw new WebException(HttpStatus.BAD_REQUEST,"Book not found");
        }

        return entity;
    }
}
