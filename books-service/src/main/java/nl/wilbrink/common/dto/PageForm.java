package nl.wilbrink.common.dto;

public class PageForm {

    private int page = 1;
    private int size = 20;
    private String sortDirection;
    private String sortField;

    public PageForm() {
    }

    public PageForm(int page, int size, String sortDirection, String sortField) {
        this.page = page;
        this.size = size;
        this.sortDirection = sortDirection;
        this.sortField = sortField;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getSortDirection() {
        return sortDirection;
    }

    public void setSortDirection(String sortDirection) {
        this.sortDirection = sortDirection;
    }

    public String getSortField() {
        return sortField;
    }

    public void setSortField(String sortField) {
        this.sortField = sortField;
    }

}