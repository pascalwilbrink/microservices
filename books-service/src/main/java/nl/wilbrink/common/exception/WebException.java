package nl.wilbrink.common.exception;

import org.springframework.http.HttpStatus;

public class WebException extends RuntimeException {

    private HttpStatus status;

    public WebException(HttpStatus status) {
        this(status, "Internal server error");
    }

    public WebException(HttpStatus status, String message) {
        this(status, message, null);
    }

    public WebException(HttpStatus status, String message, Throwable cause) {
        super(message, cause);

        this.status = status;
    }

    public HttpStatus getStatus() {
        return status;
    }
}
