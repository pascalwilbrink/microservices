-- Create Authors table
create table authors (
    id int NOT NULL PRIMARY KEY,
    name varchar(255),
    summary varchar(2000),
    version int,
    created timestamp,
    updated timestamp,
    created_by int,
    updated_by int
);

-- Create Books table
create table books (
    id int NOT NULL PRIMARY KEY,
    author_id int NOT NULL,
    title varchar(255) NOT NULL,
    summary varchar(2000),
    stock int,
    price int,
    version int,
    created timestamp,
    updated timestamp,
    created_by int,
    updated_by int,
    FOREIGN KEY (author_id) REFERENCES authors(id)
);