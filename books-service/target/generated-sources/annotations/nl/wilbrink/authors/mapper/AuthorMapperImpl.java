package nl.wilbrink.authors.mapper;

import javax.annotation.Generated;
import nl.wilbrink.authors.dto.AuthorDTO;
import nl.wilbrink.authors.entity.Author;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2017-09-19T19:22:01+0200",
    comments = "version: 1.2.0.CR2, compiler: javac, environment: Java 1.8.0_144 (Oracle Corporation)"
)
@Component
public class AuthorMapperImpl extends AuthorMapper {

    @Override
    public AuthorDTO toDTO(Author entity) {
        if ( entity == null ) {
            return null;
        }

        AuthorDTO authorDTO = new AuthorDTO();

        authorDTO.setId( entity.getId() );
        authorDTO.setVersion( entity.getVersion() );
        authorDTO.setCreated( entity.getCreated() );
        authorDTO.setUpdated( entity.getUpdated() );
        authorDTO.setCreatedBy( entity.getCreatedBy() );
        authorDTO.setUpdatedBy( entity.getUpdatedBy() );
        authorDTO.setName( entity.getName() );
        authorDTO.setSummary( entity.getSummary() );

        return authorDTO;
    }

    @Override
    public Author toEntity(AuthorDTO dto) {
        if ( dto == null ) {
            return null;
        }

        Author author = new Author();

        author.setId( dto.getId() );
        author.setVersion( dto.getVersion() );
        author.setCreated( dto.getCreated() );
        author.setUpdated( dto.getUpdated() );
        author.setCreatedBy( dto.getCreatedBy() );
        author.setUpdatedBy( dto.getUpdatedBy() );
        author.setName( dto.getName() );
        author.setSummary( dto.getSummary() );

        return author;
    }
}
