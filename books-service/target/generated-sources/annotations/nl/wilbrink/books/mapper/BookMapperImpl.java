package nl.wilbrink.books.mapper;

import javax.annotation.Generated;
import nl.wilbrink.authors.dto.AuthorDTO;
import nl.wilbrink.authors.entity.Author;
import nl.wilbrink.books.dto.BookDTO;
import nl.wilbrink.books.entity.Book;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2017-09-19T19:22:01+0200",
    comments = "version: 1.2.0.CR2, compiler: javac, environment: Java 1.8.0_144 (Oracle Corporation)"
)
@Component
public class BookMapperImpl extends BookMapper {

    @Override
    public BookDTO toDTO(Book entity) {
        if ( entity == null ) {
            return null;
        }

        BookDTO bookDTO = new BookDTO();

        bookDTO.setId( entity.getId() );
        bookDTO.setVersion( entity.getVersion() );
        bookDTO.setCreated( entity.getCreated() );
        bookDTO.setUpdated( entity.getUpdated() );
        bookDTO.setCreatedBy( entity.getCreatedBy() );
        bookDTO.setUpdatedBy( entity.getUpdatedBy() );
        bookDTO.setTitle( entity.getTitle() );
        bookDTO.setSummary( entity.getSummary() );
        bookDTO.setAuthor( authorToAuthorDTO( entity.getAuthor() ) );

        return bookDTO;
    }

    @Override
    public Book toEntity(BookDTO dto) {
        if ( dto == null ) {
            return null;
        }

        Book book = new Book();

        book.setId( dto.getId() );
        book.setVersion( dto.getVersion() );
        book.setCreated( dto.getCreated() );
        book.setUpdated( dto.getUpdated() );
        book.setCreatedBy( dto.getCreatedBy() );
        book.setUpdatedBy( dto.getUpdatedBy() );
        book.setTitle( dto.getTitle() );
        book.setSummary( dto.getSummary() );
        book.setAuthor( authorDTOToAuthor( dto.getAuthor() ) );

        return book;
    }

    protected AuthorDTO authorToAuthorDTO(Author author) {
        if ( author == null ) {
            return null;
        }

        AuthorDTO authorDTO = new AuthorDTO();

        authorDTO.setId( author.getId() );
        authorDTO.setVersion( author.getVersion() );
        authorDTO.setCreated( author.getCreated() );
        authorDTO.setUpdated( author.getUpdated() );
        authorDTO.setCreatedBy( author.getCreatedBy() );
        authorDTO.setUpdatedBy( author.getUpdatedBy() );
        authorDTO.setName( author.getName() );
        authorDTO.setSummary( author.getSummary() );

        return authorDTO;
    }

    protected Author authorDTOToAuthor(AuthorDTO authorDTO) {
        if ( authorDTO == null ) {
            return null;
        }

        Author author = new Author();

        author.setId( authorDTO.getId() );
        author.setVersion( authorDTO.getVersion() );
        author.setCreated( authorDTO.getCreated() );
        author.setUpdated( authorDTO.getUpdated() );
        author.setCreatedBy( authorDTO.getCreatedBy() );
        author.setUpdatedBy( authorDTO.getUpdatedBy() );
        author.setName( authorDTO.getName() );
        author.setSummary( authorDTO.getSummary() );

        return author;
    }
}
