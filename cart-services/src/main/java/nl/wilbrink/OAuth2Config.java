package nl.wilbrink;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.http.ResponseEntity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.common.exceptions.InvalidTokenException;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.OAuth2Request;
import org.springframework.security.oauth2.provider.authentication.BearerTokenExtractor;
import org.springframework.security.oauth2.provider.authentication.TokenExtractor;
import org.springframework.security.oauth2.provider.token.*;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Serializable;
import java.util.*;

@EnableResourceServer
public class OAuth2Config extends ResourceServerConfigurerAdapter {

    @Value("${auth.server.url}")
    private String authorizationUrl;

    @Value("${auth.server.clientId}")
    private String clientId;

    @Value("${auth.server.clientSecret}")
    private String clientSecret;

    private TokenExtractor tokenExtractor = new BearerTokenExtractor();

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
        resources.tokenServices(remoteTokenServices()).resourceId("resource");
    }


    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.addFilterAfter(new OncePerRequestFilter() {
            @Override
            protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
                if(tokenExtractor.extract(request) == null) {
                    SecurityContextHolder.clearContext();;
                }

                filterChain.doFilter(request, response);
            }
        }, AbstractPreAuthenticatedProcessingFilter.class);

        http.csrf().disable();
        http.cors().disable();

        http
                .authorizeRequests()
                .anyRequest()
                .authenticated()
                ;
    }

    @Bean
    public AccessTokenConverter accessTokenConverter() {
        return new AccessTokenConverter();
    }

    @Bean
    public RemoteTokenServices remoteTokenServices() {
        final RemoteTokenServices remoteTokenServices = new MyRemoteTokenServices();
        remoteTokenServices.setCheckTokenEndpointUrl(authorizationUrl);
        remoteTokenServices.setClientId(clientId);
        remoteTokenServices.setClientSecret(clientSecret);
        remoteTokenServices.setAccessTokenConverter(accessTokenConverter());

        return remoteTokenServices;
    }

    public static class AccessTokenConverter extends DefaultAccessTokenConverter {

        private UserAuthenticationConverter userTokenConverter = new DefaultUserAuthenticationConverter();

        @Override
        public OAuth2Authentication extractAuthentication(Map<String, ?> map) {
            Map<String, String> parameters = new HashMap<String, String>();

            Set<String> scope = new LinkedHashSet<String>(map.containsKey(SCOPE) ? (Collection<String>) map.get(SCOPE)
                    : Collections.<String>emptySet());

            Authentication user = userTokenConverter.extractAuthentication(map);
            String clientId = (String) map.get(CLIENT_ID);
            parameters.put(CLIENT_ID, clientId);

            parameters.put("id",String.valueOf(map.get("id")));

            Set<String> resourceIds = new LinkedHashSet<String>(map.containsKey(AUD) ? (Collection<String>) map.get(AUD)
                    : Collections.<String>emptySet());

            OAuth2Request request = new OAuth2Request(parameters, clientId, null, true, scope, resourceIds, null, null,
                    null);
            return new OAuth2Authentication(request, user);
        }

    }

    public static class MyRemoteTokenServices extends RemoteTokenServices {

        public OAuth2Authentication loadAuthentication(String accessToken) throws AuthenticationException, InvalidTokenException {
            OAuth2Authentication auth = null;

            try {
                auth = super.loadAuthentication( accessToken );
            } catch( Exception e ) {
                System.out.println("error");
            }

            return auth;
        }

    }

}
