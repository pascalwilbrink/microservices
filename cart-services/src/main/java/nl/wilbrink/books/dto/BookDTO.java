package nl.wilbrink.books.dto;

import nl.wilbrink.common.dto.BaseDTO;

import java.io.Serializable;

public class BookDTO extends BaseDTO implements Serializable {

    private String title;

    private String summary;

    private AuthorDTO author;

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getSummary() {
        return summary;
    }

    public void setAuthor(AuthorDTO author) {
        this.author = author;
    }

    public AuthorDTO getAuthor() {
        return author;
    }
}
