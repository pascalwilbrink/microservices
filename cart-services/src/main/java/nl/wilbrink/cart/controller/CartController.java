package nl.wilbrink.cart.controller;

import nl.wilbrink.books.dto.BookDTO;
import nl.wilbrink.cart.dto.CartItemDTO;
import nl.wilbrink.cart.service.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/cart")
public class CartController {

    private final CartService cartService;

    @Autowired
    public CartController(CartService cartService) {
        this.cartService = cartService;
    }

    @RequestMapping
    public ResponseEntity getCart() {
        List<CartItemDTO> cart = cartService.getCart();

        return ResponseEntity.ok(cart);
    }

    @RequestMapping(method= RequestMethod.POST)
    public ResponseEntity addToCart(@RequestBody CartItemDTO item) {
        this.cartService.addtoCart(item);
        return getCart();
    }
}
