package nl.wilbrink.cart.dto;

import nl.wilbrink.books.dto.BookDTO;
import nl.wilbrink.common.dto.BaseDTO;

import java.io.Serializable;

public class CartItemDTO extends BaseDTO implements Serializable {

    private BookDTO book;

    private Long amount;

    private Long price;

    public void setBook(BookDTO book) {
        this.book = book;
    }

    public BookDTO getBook() {
        return book;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public Long getAmount() {
        return amount;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public Long getPrice() {
        return price;
    }

}
