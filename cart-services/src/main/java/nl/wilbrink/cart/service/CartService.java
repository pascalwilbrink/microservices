package nl.wilbrink.cart.service;

import nl.wilbrink.cart.dto.CartItemDTO;
import nl.wilbrink.cart.store.CartStore;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CartService {

    private final CartStore cartStore;

    public CartService(CartStore cartStore) {
        this.cartStore = cartStore;
    }

    public List<CartItemDTO> getCart() {
        return cartStore.getCart((String)SecurityContextHolder.getContext().getAuthentication().getPrincipal());
    }

    public void addtoCart(CartItemDTO item) {
        cartStore.addToCart((String)SecurityContextHolder.getContext().getAuthentication().getPrincipal(),item);
    }
}
