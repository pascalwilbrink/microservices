package nl.wilbrink.cart.store;

import nl.wilbrink.cart.dto.CartItemDTO;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@Component
public class CartStore {

    private ConcurrentMap<String,List<CartItemDTO>> store = new ConcurrentHashMap<>();

    public void addToCart(String username, CartItemDTO item) {
        if(!store.containsKey(username)) {
            store.put(username,new ArrayList<>());
        }

        Optional<CartItemDTO> optCartItem = store.get(username).stream().filter(cartItem -> cartItem.getBook().getId().equals(item.getBook().getId())).findAny();

        if(optCartItem.isPresent()) {
            optCartItem.get().setAmount(optCartItem.get().getAmount() + item.getAmount());
        } else {
            store.get(username).add(item);
        }
    }

    public List<CartItemDTO> getCart(String username) {
        return store.get(username);
    }
}
