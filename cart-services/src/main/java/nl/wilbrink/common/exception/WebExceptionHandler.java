package nl.wilbrink.common.exception;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class WebExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value={WebException.class})
    protected ResponseEntity handleException(WebException exception, WebRequest request) {
        ExceptionMessage exceptionMessage = new ExceptionMessage();
        exceptionMessage.setMessage(exception.getMessage());
        exceptionMessage.setUrl(request.getContextPath());

        return ResponseEntity.status(exception.getStatus()).body(exceptionMessage);
    }

}