package nl.wilbrink.actors.controller;

import nl.wilbrink.actors.dto.ActorDTO;
import nl.wilbrink.actors.service.ActorsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/actors")
public class ActorsController {

    private final ActorsService actorsService;

    @Autowired
    public ActorsController(ActorsService actorsService) {
        this.actorsService = actorsService;
    }

    @RequestMapping
    public List<ActorDTO> getActors() {
        List<ActorDTO> actors = actorsService.getActors();

        return actors;
    }

    @RequestMapping(value= "/{id}")
    public ActorDTO getActor(@PathVariable("id") Long id) {
        ActorDTO actor = actorsService.getActor(id);

        return actor;
    }

    @RequestMapping(method= RequestMethod.POST)
    public ActorDTO createActor(@RequestBody ActorDTO actor) {
        actor = actorsService.createActor(actor);

        return actor;
    }

    @RequestMapping(value= "/{id}", method= RequestMethod.PUT)
    public ActorDTO updateActor(@PathVariable("id") Long id, ActorDTO actor) {
        actor.setId(id);

        actor = actorsService.updateActor(actor);

        return actor;
    }

    @RequestMapping(value= "/{id}", method= RequestMethod.DELETE)
    public void deleteActor(@PathVariable("id") Long id) {
        actorsService.deleteActor(id);
    }
}
