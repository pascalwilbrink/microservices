package nl.wilbrink.actors.dto;

import java.io.Serializable;

public class ActorDTO implements Serializable {

    private Long id;

    private String name;

    private String photo;

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getPhoto() {
        return photo;
    }

}
