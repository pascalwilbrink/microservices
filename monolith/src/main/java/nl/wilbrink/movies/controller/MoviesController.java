package nl.wilbrink.movies.controller;

import nl.wilbrink.movies.dto.MovieDTO;
import nl.wilbrink.movies.service.MoviesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
@RequestMapping("/movies")
public class MoviesController {

    private final MoviesService moviesService;

    @Autowired
    public MoviesController(MoviesService moviesService) {
        this.moviesService = moviesService;
    }

    @RequestMapping
    public ResponseEntity getMovies() {
        return ResponseEntity.ok( moviesService.getMovies() );
    }

    @RequestMapping(value="/{id}")
    public ResponseEntity getMovie(@PathVariable("id") Long id) {
        return ResponseEntity.ok( moviesService.getMovie(id) );
    }

    @RequestMapping(method=RequestMethod.POST)
    public ResponseEntity createMovie(@RequestBody MovieDTO movie) {
        movie = moviesService.createMovie(movie);

        return ResponseEntity.ok( movie );
    }

    @RequestMapping(value="/{id}", method= RequestMethod.PUT)
    public ResponseEntity updateMovie(@PathVariable("id") Long id, @RequestBody MovieDTO movie) {
        movie.setId(id);

        movie = moviesService.updateMovie(movie);

        return ResponseEntity.ok( movie );
    }

    @RequestMapping(value="/{id}", method=RequestMethod.DELETE)
    public void deleteMovie(@PathVariable("id") Long id) {
        moviesService.deleteMovie(id);
    }
}
