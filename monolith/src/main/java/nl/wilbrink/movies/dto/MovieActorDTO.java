package nl.wilbrink.movies.dto;

import nl.wilbrink.actors.dto.ActorDTO;

import java.io.Serializable;

public class MovieActorDTO implements Serializable {

    private Long id;

    private String name;

    private ActorDTO actor;

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setActor(ActorDTO actor) {
        this.actor = actor;
    }

    public ActorDTO getActor() {
        return actor;
    }

}
