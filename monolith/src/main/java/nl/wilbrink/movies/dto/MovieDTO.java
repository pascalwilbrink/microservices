package nl.wilbrink.movies.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MovieDTO implements Serializable {

    private Long id;

    private String title;

    private Long releaseYear;

    private String plot;

    private String poster;

    private List<MovieActorDTO> movieActors = new ArrayList<>();

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setReleaseYear(Long releaseYear) {
        this.releaseYear = releaseYear;
    }

    public Long getReleaseYear() {
        return releaseYear;
    }

    public void setPlot(String plot) {
        this.plot = plot;
    }

    public String getPlot() {
        return plot;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public String getPoster() {
        return poster;
    }

    public void setMovieActors(List<MovieActorDTO> movieActors) {
        this.movieActors = movieActors;
    }

    public List<MovieActorDTO> getMovieActors() {
        return movieActors;
    }

}
