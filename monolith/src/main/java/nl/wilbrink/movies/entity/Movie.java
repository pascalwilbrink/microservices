package nl.wilbrink.movies.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "movies")
public class Movie implements Serializable {

    @Id
    @GeneratedValue
    @Column
    private Long id;

    @Column
    private String title;

    @Column(name = "release_year")
    private Long releaseYear;

    @Column(name = "plot")
    private String plot;

    @Column(name = "poster")
    private String poster;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "movie")
    private Set<MovieActor> movieActors = new HashSet<>();

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setReleaseYear(Long releaseYear) {
        this.releaseYear = releaseYear;
    }

    public Long getReleaseYear() {
        return releaseYear;
    }

    public void setPlot(String plot) {
        this.plot = plot;
    }

    public String getPlot() {
        return plot;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public String getPoster() {
        return poster;
    }

    public void setMovieActors(Set<MovieActor> movieActors) {
        this.movieActors = movieActors;
    }

    public Set<MovieActor> getMovieActors() {
        return movieActors;
    }

    public void addMovieActor(MovieActor movieActor) {
        movieActor.setMovie(this);

        this.movieActors.add(movieActor);
    }
}
