package nl.wilbrink.movies.mapper;

import nl.wilbrink.actors.mapper.ActorMapper;
import nl.wilbrink.movies.dto.MovieActorDTO;
import nl.wilbrink.movies.entity.MovieActor;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {ActorMapper.class })
public abstract class MovieActorMapper {

    public abstract MovieActorDTO toDTO(MovieActor entity);

    public abstract MovieActor toEntity(MovieActorDTO dto);

}
