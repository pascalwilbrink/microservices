package nl.wilbrink.movies.mapper;

import nl.wilbrink.movies.dto.MovieDTO;
import nl.wilbrink.movies.entity.Movie;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = { MovieActorMapper.class })
public abstract class MovieMapper {

    public abstract MovieDTO toDTO(Movie entity);

    public abstract Movie toEntity(MovieDTO dto);

}
