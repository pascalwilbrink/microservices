package nl.wilbrink.movies.service;

import nl.wilbrink.movies.dto.MovieDTO;
import nl.wilbrink.movies.entity.Movie;
import nl.wilbrink.movies.mapper.MovieMapper;
import nl.wilbrink.movies.repository.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MoviesService {

    private final MovieRepository movieRepository;

    private final MovieMapper movieMapper;

    @Autowired
    public MoviesService(MovieRepository movieRepository, MovieMapper movieMapper) {
        this.movieRepository = movieRepository;
        this.movieMapper = movieMapper;
    }

    public List<MovieDTO> getMovies() {
        List<MovieDTO> movies = new ArrayList<>();

        movieRepository
                .findAll()
                .forEach(movie->{
                    movies.add(movieMapper.toDTO(movie));
                });

        return movies;
    }

    public MovieDTO getMovie(Long id) {
        Movie entity = movieRepository.findOne(id);

        return movieMapper.toDTO(entity);
    }

    public MovieDTO createMovie(MovieDTO movie) {
        Movie entity = movieMapper.toEntity(movie);

        entity = movieRepository.save(entity);

        return movieMapper.toDTO(entity);
    }

    public MovieDTO updateMovie(MovieDTO movie) {
        Movie entity = movieMapper.toEntity(movie);

        entity = movieRepository.save(entity);

        return movieMapper.toDTO(entity);
    }

    public void deleteMovie(Long id) {
        movieRepository.delete(id);
    }
}
