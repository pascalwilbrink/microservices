insert into actors(id,name,photo) values(1, 'Keanu Reeves', 'https://images-na.ssl-images-amazon.com/images/M/MV5BNjUxNDcwMTg4Ml5BMl5BanBnXkFtZTcwMjU4NDYyOA@@._V1_SY1000_CR0,0,771,1000_AL_.jpg');
insert into actors(id,name,photo) values(2, 'Laurence Fishburne', 'https://images-na.ssl-images-amazon.com/images/M/MV5BMTc0NjczNDc1MV5BMl5BanBnXkFtZTYwMDU0Mjg1._V1_.jpg');
insert into actors(id,name,photo) values(3, 'Carrie-Anne Moss', 'https://images-na.ssl-images-amazon.com/images/M/MV5BMTYxMjgwNzEwOF5BMl5BanBnXkFtZTcwNTQ0NzI5Ng@@._V1_SY1000_CR0,0,755,1000_AL_.jpg');
insert into actors(id,name,photo) values(4, 'Hugo Weaving', 'https://images-na.ssl-images-amazon.com/images/M/MV5BMTUwMTY4OTA5MV5BMl5BanBnXkFtZTcwNTE1MTY0OA@@._V1_SY1000_CR0,0,690,1000_AL_.jpg');
insert into actors(id,name,photo) values(5, 'Helmut Bakaitis', 'https://images-na.ssl-images-amazon.com/images/M/MV5BOTJmOTY5YzUtMDRhOC00MTI3LTkwZTUtNjk2YmUyYTljNzc3XkEyXkFqcGdeQXVyMzI2MDEwNA@@._V1_.jpg');
insert into actors(id,name,photo) values(6, 'Mary Alice', 'https://images-na.ssl-images-amazon.com/images/M/MV5BMTYwMTI4MTM2Nl5BMl5BanBnXkFtZTcwMDU1MDk5Nw@@._V1_SY1000_CR0,0,775,1000_AL_.jpg');
