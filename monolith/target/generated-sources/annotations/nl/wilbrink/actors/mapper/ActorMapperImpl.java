package nl.wilbrink.actors.mapper;

import javax.annotation.Generated;
import nl.wilbrink.actors.dto.ActorDTO;
import nl.wilbrink.actors.entity.Actor;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2017-09-16T15:05:19+0200",
    comments = "version: 1.2.0.CR2, compiler: javac, environment: Java 1.8.0_144 (Oracle Corporation)"
)
@Component
public class ActorMapperImpl extends ActorMapper {

    @Override
    public ActorDTO toDTO(Actor entity) {
        if ( entity == null ) {
            return null;
        }

        ActorDTO actorDTO = new ActorDTO();

        actorDTO.setId( entity.getId() );
        actorDTO.setName( entity.getName() );
        actorDTO.setPhoto( entity.getPhoto() );

        return actorDTO;
    }

    @Override
    public Actor toEntity(ActorDTO dto) {
        if ( dto == null ) {
            return null;
        }

        Actor actor = new Actor();

        actor.setId( dto.getId() );
        actor.setName( dto.getName() );
        actor.setPhoto( dto.getPhoto() );

        return actor;
    }
}
