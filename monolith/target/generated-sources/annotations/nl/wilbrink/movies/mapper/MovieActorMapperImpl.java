package nl.wilbrink.movies.mapper;

import javax.annotation.Generated;
import nl.wilbrink.actors.mapper.ActorMapper;
import nl.wilbrink.movies.dto.MovieActorDTO;
import nl.wilbrink.movies.entity.MovieActor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2017-09-16T15:05:19+0200",
    comments = "version: 1.2.0.CR2, compiler: javac, environment: Java 1.8.0_144 (Oracle Corporation)"
)
@Component
public class MovieActorMapperImpl extends MovieActorMapper {

    @Autowired
    private ActorMapper actorMapper;

    @Override
    public MovieActorDTO toDTO(MovieActor entity) {
        if ( entity == null ) {
            return null;
        }

        MovieActorDTO movieActorDTO = new MovieActorDTO();

        movieActorDTO.setId( entity.getId() );
        movieActorDTO.setName( entity.getName() );
        movieActorDTO.setActor( actorMapper.toDTO( entity.getActor() ) );

        return movieActorDTO;
    }

    @Override
    public MovieActor toEntity(MovieActorDTO dto) {
        if ( dto == null ) {
            return null;
        }

        MovieActor movieActor = new MovieActor();

        movieActor.setId( dto.getId() );
        movieActor.setName( dto.getName() );
        movieActor.setActor( actorMapper.toEntity( dto.getActor() ) );

        return movieActor;
    }
}
