package nl.wilbrink.movies.mapper;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.annotation.Generated;
import nl.wilbrink.movies.dto.MovieActorDTO;
import nl.wilbrink.movies.dto.MovieDTO;
import nl.wilbrink.movies.entity.Movie;
import nl.wilbrink.movies.entity.MovieActor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2017-09-16T15:05:19+0200",
    comments = "version: 1.2.0.CR2, compiler: javac, environment: Java 1.8.0_144 (Oracle Corporation)"
)
@Component
public class MovieMapperImpl extends MovieMapper {

    @Autowired
    private MovieActorMapper movieActorMapper;

    @Override
    public MovieDTO toDTO(Movie entity) {
        if ( entity == null ) {
            return null;
        }

        MovieDTO movieDTO = new MovieDTO();

        movieDTO.setId( entity.getId() );
        movieDTO.setTitle( entity.getTitle() );
        movieDTO.setReleaseYear( entity.getReleaseYear() );
        movieDTO.setPlot( entity.getPlot() );
        movieDTO.setPoster( entity.getPoster() );
        movieDTO.setMovieActors( movieActorSetToMovieActorDTOList( entity.getMovieActors() ) );

        return movieDTO;
    }

    @Override
    public Movie toEntity(MovieDTO dto) {
        if ( dto == null ) {
            return null;
        }

        Movie movie = new Movie();

        movie.setId( dto.getId() );
        movie.setTitle( dto.getTitle() );
        movie.setReleaseYear( dto.getReleaseYear() );
        movie.setPlot( dto.getPlot() );
        movie.setPoster( dto.getPoster() );
        movie.setMovieActors( movieActorDTOListToMovieActorSet( dto.getMovieActors() ) );

        return movie;
    }

    protected List<MovieActorDTO> movieActorSetToMovieActorDTOList(Set<MovieActor> set) {
        if ( set == null ) {
            return null;
        }

        List<MovieActorDTO> list = new ArrayList<MovieActorDTO>( set.size() );
        for ( MovieActor movieActor : set ) {
            list.add( movieActorMapper.toDTO( movieActor ) );
        }

        return list;
    }

    protected Set<MovieActor> movieActorDTOListToMovieActorSet(List<MovieActorDTO> list) {
        if ( list == null ) {
            return null;
        }

        Set<MovieActor> set = new HashSet<MovieActor>( Math.max( (int) ( list.size() / .75f ) + 1, 16 ) );
        for ( MovieActorDTO movieActorDTO : list ) {
            set.add( movieActorMapper.toEntity( movieActorDTO ) );
        }

        return set;
    }
}
