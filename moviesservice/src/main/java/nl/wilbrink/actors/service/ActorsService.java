package nl.wilbrink.actors.service;

import feign.hystrix.FallbackFactory;
import nl.wilbrink.actors.dto.ActorDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Collections;
import java.util.List;

@Service
public class ActorsService {

    private final ActorsResource actorsResource;

    @Autowired
    public ActorsService(ActorsResource actorsResource) {
        this.actorsResource = actorsResource;
    }

    public List<ActorDTO> getActors() {
        List<ActorDTO> actors = actorsResource.getActors();

        return actors;
    }

    public ActorDTO getActor(Long id) {
        ActorDTO actor = actorsResource.getActor(id);

        return actor;
    }

    @FeignClient(
        value = "actors-service",
        fallbackFactory = ActorsResource.HystrixActorsFallbackFactory.class
    )
    public interface ActorsResource {

        @RequestMapping(value="/actors")
        List<ActorDTO> getActors();

        @RequestMapping(value="actors/{id}")
        ActorDTO getActor(@PathVariable("id") Long id);

        @Component
        public static class HystrixActorsFallbackFactory implements FallbackFactory<ActorsResource> {

            @Override
            public ActorsResource create(Throwable cause) {
                return new ActorsResource() {
                    @Override
                    public List<ActorDTO> getActors() {
                        return Collections.emptyList();
                    }

                    @Override
                    public ActorDTO getActor(Long id) {
                        ActorDTO actor = new ActorDTO();
                        actor.setName(cause.getMessage());
                        actor.setId(id);

                        return actor;
                    }
                };
            }
        }

    }
}
