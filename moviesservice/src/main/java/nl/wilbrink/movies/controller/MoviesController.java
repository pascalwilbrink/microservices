package nl.wilbrink.movies.controller;

import nl.wilbrink.movies.dto.MovieDTO;
import nl.wilbrink.movies.dto.SearchMovieRequest;
import nl.wilbrink.movies.service.MoviesService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/movies")
public class MoviesController {

    private final MoviesService moviesService;

    public MoviesController(MoviesService moviesService) {
        this.moviesService = moviesService;
    }

    @RequestMapping("/{id}")
    public ResponseEntity getMovie(@PathVariable("id") Long id) {
        MovieDTO movie = moviesService.getMovie(id);

        return ResponseEntity.ok(movie);
    }

    @RequestMapping(value="/search", method=RequestMethod.POST)
    public ResponseEntity searchMovies(@RequestBody SearchMovieRequest searchRequest) {
        List<MovieDTO> movies = moviesService.searchMovies(searchRequest);

        return ResponseEntity.ok(movies);
    }

}
