package nl.wilbrink.movies.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MovieDTO implements Serializable {

    private Long id;

    private String title;

    private String poster;

    private Long releaseYear;

    private String plot;

    private List<MovieActorDTO> actors = new ArrayList<>();

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public String getPoster() {
        return poster;
    }

    public void setReleaseYear(Long releaseYear) {
        this.releaseYear = releaseYear;
    }

    public Long getReleaseYear() {
        return releaseYear;
    }

    public void setPlot(String plot) {
        this.plot = plot;
    }

    public String getPlot() {
        return plot;
    }

    public void setActors(List<MovieActorDTO> actors) {
        this.actors = actors;
    }

    public List<MovieActorDTO> getActors() {
        return actors;
    }

    public void addActor(MovieActorDTO actor) {
        this.actors.add(actor);
    }
}
