package nl.wilbrink.movies.dto;

import javax.validation.constraints.Min;
import java.io.Serializable;

public class SearchMovieRequest implements Serializable {

    private String query;

    @Min(1)
    private int page = 1;

    private int pageSize = 20;

    public void setQuery(String query) {
        this.query = query;
    }

    public String getQuery() {
        return query;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPage() {
        return page;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getPageSize() {
        return pageSize;
    }

}
