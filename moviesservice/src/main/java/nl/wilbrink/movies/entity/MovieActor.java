package nl.wilbrink.movies.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "movie_actors")
public class MovieActor implements Serializable {

    @Id
    @GeneratedValue
    @Column
    private Long id;

    @Column
    private String name;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "movie_id")
    private Movie movie;

    @Column(name = "actor_id")
    private Long actorId;

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public Movie getMovie() {
        return movie;
    }

    public void setActorId(Long actorId) {
        this.actorId = actorId;
    }

    public Long getActorId() {
        return actorId;
    }
}