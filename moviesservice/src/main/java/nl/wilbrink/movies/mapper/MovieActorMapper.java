package nl.wilbrink.movies.mapper;


import nl.wilbrink.actors.service.ActorsService;
import nl.wilbrink.movies.dto.MovieActorDTO;
import nl.wilbrink.movies.entity.MovieActor;
import nl.wilbrink.movies.entity.tmdb.TMDBGetMovieCreditsResponse;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(componentModel = "spring")
public abstract class MovieActorMapper {

    @Mapping(target="name",source="character")
    @Mapping(target="actor",ignore=true)
    @Mapping(target="actor.id",source="id")
    @Mapping(target="actor.name",source="name")
    @Mapping(target="actor.photo",ignore=true)
    public abstract MovieActorDTO toDTO(TMDBGetMovieCreditsResponse.Cast actor);

    @AfterMapping
    protected void afterMapping(TMDBGetMovieCreditsResponse.Cast entity, @MappingTarget MovieActorDTO dto) {
        if(entity.getProfilePath() != null && !entity.getProfilePath().equals("")) {
            dto.getActor().setPhoto(String.format("https://image.tmdb.org/t/p/w500/%s",entity.getProfilePath()));
        }
    }

}
