package nl.wilbrink.movies.mapper;

import nl.wilbrink.movies.dto.MovieDTO;
import nl.wilbrink.movies.entity.MovieActor;
import nl.wilbrink.movies.entity.tmdb.TMDBGetMovieCreditsResponse;
import nl.wilbrink.movies.entity.tmdb.TMDBGetMovieResponse;
import nl.wilbrink.movies.entity.tmdb.TMDBSearchMovieResponse;
import nl.wilbrink.movies.service.MoviesService;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

@Mapper(componentModel = "spring")
public abstract class MovieMapper {

    @Value("${tmdb.api.key}")
    private String apiKey;

    @Autowired
    private MoviesService.TMDBMoviesResource tmdbMoviesResource;

    @Autowired
    private MovieActorMapper movieActorMapper;

    @Mapping(target="id",source="id")
    @Mapping(target="title",source="title")
    @Mapping(target="poster",ignore=true)
    @Mapping(target="releaseYear",ignore=true)
    @Mapping(target="plot",source="overview")
    public abstract MovieDTO toDTO(TMDBSearchMovieResponse.Result entity);

    @Mapping(target="id",source="id")
    @Mapping(target="title",source="title")
    @Mapping(target="poster",ignore=true)
    @Mapping(target="releaseYear",ignore=true)
    @Mapping(target="plot",source="overview")
    public abstract MovieDTO toDTO(TMDBGetMovieResponse entity);

    @AfterMapping
    protected void afterMapping(TMDBGetMovieResponse entity, @MappingTarget MovieDTO dto) {
        if(entity.getReleaseDate() != null && !entity.getReleaseDate().equals("")) {
            dto.setReleaseYear(Long.valueOf(entity.getReleaseDate().substring(0,4)));
        }

        if(entity.getPosterPath() != null && !entity.getPosterPath().equals("")) {
            dto.setPoster(String.format("https://image.tmdb.org/t/p/w500/%s", entity.getPosterPath()));
        }
    }

    @AfterMapping
    protected void afterMapping(TMDBSearchMovieResponse.Result entity, @MappingTarget MovieDTO dto) {
        if(entity.getReleaseDate() != null && !entity.getReleaseDate().equals("")) {
            dto.setReleaseYear(Long.valueOf(entity.getReleaseDate().substring(0,4)));
        }

        if(entity.getPosterPath() != null && !entity.getPosterPath().equals("")) {
            dto.setPoster(String.format("https://image.tmdb.org/t/p/w500/%s", entity.getPosterPath()));
        }

    }

    @AfterMapping
    protected void addActors(TMDBGetMovieResponse entity, @MappingTarget MovieDTO dto) {
        TMDBGetMovieCreditsResponse credits = tmdbMoviesResource.getMovieCredits(dto.getId(),apiKey);

        credits.getCast().forEach(cast -> {
            dto.addActor(movieActorMapper.toDTO(cast));
        });

    }


}