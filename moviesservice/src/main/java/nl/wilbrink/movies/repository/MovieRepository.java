package nl.wilbrink.movies.repository;

import nl.wilbrink.movies.entity.Movie;
import org.springframework.data.repository.CrudRepository;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping
public interface MovieRepository extends CrudRepository<Movie,Long> {
}
