package nl.wilbrink.movies.service;

import feign.hystrix.FallbackFactory;
import nl.wilbrink.movies.dto.MovieDTO;
import nl.wilbrink.movies.dto.SearchMovieRequest;
import nl.wilbrink.movies.entity.tmdb.TMDBGetMovieCreditsResponse;
import nl.wilbrink.movies.entity.tmdb.TMDBGetMovieResponse;
import nl.wilbrink.movies.entity.tmdb.TMDBSearchMovieResponse;
import nl.wilbrink.movies.mapper.MovieMapper;
import nl.wilbrink.movies.repository.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;

@Service
public class MoviesService {

    private final MovieRepository movieRepository;

    private final MovieMapper movieMapper;

    private final TMDBMoviesResource tmdbMoviesResource;

    @Value("${tmdb.api.key}")
    private String apiKey;

    @Autowired
    public MoviesService(MovieRepository movieRepository, MovieMapper movieMapper, TMDBMoviesResource tmdbMoviesResource) {
        this.movieRepository = movieRepository;
        this.movieMapper = movieMapper;
        this.tmdbMoviesResource = tmdbMoviesResource;
    }

    public List<MovieDTO> searchMovies(SearchMovieRequest searchRequest) {
        String language = "en-US";
        int page = searchRequest.getPage();

        TMDBSearchMovieResponse response = tmdbMoviesResource.searchMovies(apiKey,language,searchRequest.getQuery(),page,false);

        List<MovieDTO> movies = new ArrayList<>();

        response.getResults().forEach(result -> {
            movies.add(movieMapper.toDTO(result));
        });

        return movies;
    }

    public MovieDTO getMovie(Long id) {
        String language = "en-US";
        TMDBGetMovieResponse response = tmdbMoviesResource.getMovie(id,apiKey,language);

        MovieDTO movie = movieMapper.toDTO(response);

        return movie;
    }

    @FeignClient(
            url = "https://api.themoviedb.org",
            fallbackFactory = TMDBMoviesResource.HystrixMoviesFallbackFactory.class,
            name="TMDB-service"
    )
    public interface TMDBMoviesResource {

        @RequestMapping("/3/search/movie")
        TMDBSearchMovieResponse searchMovies(@RequestParam("api_key") String apiKey, @RequestParam("language") String language, @RequestParam("query") String query, @RequestParam("page") int page, @RequestParam("include_adult") boolean includeAdult);

        @RequestMapping("/3/movie/{id}")
        TMDBGetMovieResponse getMovie(@PathVariable("id") Long id, @RequestParam("api_key") String apiKey, @RequestParam("language") String language);

        @RequestMapping("/3/movie/{id}/credits")
        TMDBGetMovieCreditsResponse getMovieCredits(@PathVariable("id") Long id, @RequestParam("api_key") String apiKey);

        @Component
        class HystrixMoviesFallbackFactory implements FallbackFactory<TMDBMoviesResource> {

            @Override
            public TMDBMoviesResource create(Throwable cause) {
                return new TMDBMoviesResource() {

                    @Override
                    public TMDBSearchMovieResponse searchMovies(String apiKey, String language, String query, int page, boolean includeAdult) {
                        return new TMDBSearchMovieResponse();
                    }

                    @Override
                    public TMDBGetMovieResponse getMovie(Long id, String apiKey, String language) {
                        return new TMDBGetMovieResponse();
                    }

                    @Override
                    public TMDBGetMovieCreditsResponse getMovieCredits(Long id, String apiKey) {
                        return new TMDBGetMovieCreditsResponse();
                    }

                };
            }
        }

    }

}
