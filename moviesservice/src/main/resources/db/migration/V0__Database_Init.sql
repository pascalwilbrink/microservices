-- Create movies table
create table movies (
    id int NOT NULL PRIMARY KEY,
    title varchar(255) NOT NULL,
    release_year int,
    plot varchar(2000),
    poster varchar(500)
);

create table movie_actors (
    id int NOT NULL PRIMARY KEY,
    movie_id int NOT NULL,
    actor_id int NOT NULL,
    name varchar(255),

    FOREIGN KEY (movie_id) references movies(id)
);
