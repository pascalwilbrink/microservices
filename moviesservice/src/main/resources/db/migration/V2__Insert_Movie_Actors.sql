-- The Matrix
insert into movie_actors(id,movie_id,actor_id,name) values(1,1,1,'Neo');
insert into movie_actors(id,movie_id,actor_id,name) values(2,1,2,'Morpheus');
insert into movie_actors(id,movie_id,actor_id,name) values(3,1,3,'Trinity');
insert into movie_actors(id,movie_id,actor_id,name) values(4,1,4,'Agent Smith');

-- The Matrix Reloaded
insert into movie_actors(id,movie_id,actor_id,name) values(5,2,1,'Neo');
insert into movie_actors(id,movie_id,actor_id,name) values(6,2,2,'Morpheus');
insert into movie_actors(id,movie_id,actor_id,name) values(7,2,3,'Trinity');
insert into movie_actors(id,movie_id,actor_id,name) values(8,2,4,'Agent Smith');
insert into movie_actors(id,movie_id,actor_id,name) values(9,2,5,'The Architect');

-- The Matrix Revolutions
insert into movie_actors(id,movie_id,actor_id,name) values(10,3,1,'Neo');
insert into movie_actors(id,movie_id,actor_id,name) values(11,3,2,'Morpheus');
insert into movie_actors(id,movie_id,actor_id,name) values(12,3,3,'Trinity');
insert into movie_actors(id,movie_id,actor_id,name) values(13,3,4,'Agent Smith');
insert into movie_actors(id,movie_id,actor_id,name) values(14,3,5,'The Architect');
insert into movie_actors(id,movie_id,actor_id,name) values(15,3,6,'The Oracle');