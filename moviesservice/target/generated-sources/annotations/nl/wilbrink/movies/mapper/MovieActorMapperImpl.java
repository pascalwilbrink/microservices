package nl.wilbrink.movies.mapper;

import javax.annotation.Generated;
import nl.wilbrink.actors.dto.ActorDTO;
import nl.wilbrink.movies.dto.MovieActorDTO;
import nl.wilbrink.movies.entity.tmdb.TMDBGetMovieCreditsResponse.Cast;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2017-09-17T23:08:26+0200",
    comments = "version: 1.2.0.CR2, compiler: javac, environment: Java 1.8.0_144 (Oracle Corporation)"
)
@Component
public class MovieActorMapperImpl extends MovieActorMapper {

    @Override
    public MovieActorDTO toDTO(Cast actor) {
        if ( actor == null ) {
            return null;
        }

        MovieActorDTO movieActorDTO = new MovieActorDTO();

        movieActorDTO.setActor( castToActorDTO( actor ) );
        movieActorDTO.setName( actor.getCharacter() );

        afterMapping( actor, movieActorDTO );

        return movieActorDTO;
    }

    protected ActorDTO castToActorDTO(Cast cast) {
        if ( cast == null ) {
            return null;
        }

        ActorDTO actorDTO = new ActorDTO();

        actorDTO.setId( cast.getId() );
        actorDTO.setName( cast.getName() );

        return actorDTO;
    }
}
