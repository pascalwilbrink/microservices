package nl.wilbrink.movies.mapper;

import javax.annotation.Generated;
import nl.wilbrink.movies.dto.MovieDTO;
import nl.wilbrink.movies.entity.tmdb.TMDBGetMovieResponse;
import nl.wilbrink.movies.entity.tmdb.TMDBSearchMovieResponse.Result;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2017-09-17T23:08:25+0200",
    comments = "version: 1.2.0.CR2, compiler: javac, environment: Java 1.8.0_144 (Oracle Corporation)"
)
@Component
public class MovieMapperImpl extends MovieMapper {

    @Override
    public MovieDTO toDTO(Result entity) {
        if ( entity == null ) {
            return null;
        }

        MovieDTO movieDTO = new MovieDTO();

        movieDTO.setId( (long) entity.getId() );
        movieDTO.setTitle( entity.getTitle() );
        movieDTO.setPlot( entity.getOverview() );

        afterMapping( entity, movieDTO );

        return movieDTO;
    }

    @Override
    public MovieDTO toDTO(TMDBGetMovieResponse entity) {
        if ( entity == null ) {
            return null;
        }

        MovieDTO movieDTO = new MovieDTO();

        movieDTO.setId( entity.getId() );
        movieDTO.setTitle( entity.getTitle() );
        movieDTO.setPlot( entity.getOverview() );

        afterMapping( entity, movieDTO );
        addActors( entity, movieDTO );

        return movieDTO;
    }
}
