package nl.wilbrink.books.service;

import feign.hystrix.FallbackFactory;
import nl.wilbrink.books.dto.BookDTO;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient(
        value = "books-service",
        fallbackFactory = BooksService.HystrixBooksFallbackFactory.class
)
public interface BooksService {

    @RequestMapping("/books/{id}")
    BookDTO getBook(@PathVariable("id") Long id);

    @Component
    class HystrixBooksFallbackFactory implements FallbackFactory<BooksService> {

        @Override
        public BooksService create(Throwable cause) {

            return new BooksService() {
                @Override
                public BookDTO getBook(Long id) {
                    return null;
                }
            };
        }

    }
}