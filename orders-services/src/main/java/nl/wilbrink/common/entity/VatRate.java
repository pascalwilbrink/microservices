package nl.wilbrink.common.entity;

public enum VatRate {
    NONE(0),
    LOW(6),
    HIGH(21)
    ;

    private int rate;

    VatRate(int rate) {
        this.rate = rate;
    }

    public int getRate() {
        return rate;
    }

}
