package nl.wilbrink.invoices.dto;

import nl.wilbrink.common.dto.BaseDTO;

import java.util.ArrayList;
import java.util.List;

public class InvoiceDTO extends BaseDTO {

    private List<InvoiceLineDTO> invoiceLines = new ArrayList<>();

    public void setInvoiceLines(List<InvoiceLineDTO> invoiceLines) {
        this.invoiceLines = invoiceLines;
    }

    public List<InvoiceLineDTO> getInvoiceLines() {
        return invoiceLines;
    }

}
