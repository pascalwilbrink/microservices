package nl.wilbrink.invoices.dto;

import nl.wilbrink.common.dto.BaseDTO;

import java.io.Serializable;

public class InvoiceLineDTO extends BaseDTO implements Serializable {

    private String description;

    private Long amount;

    private Double priceExVat;

    private String vatRate;

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public Long getAmount() {
        return amount;
    }

    public void setPriceExVat(Double priceExVat) {
        this.priceExVat = priceExVat;
    }

    public Double getPriceExVat() {
        return priceExVat;
    }

    public void setVatRate(String vatRate) {
        this.vatRate = vatRate;
    }

    public String getVatRate() {
        return vatRate;
    }

}


