package nl.wilbrink.invoices.entity;

import nl.wilbrink.common.entity.BaseEntity;
import nl.wilbrink.orders.entity.Order;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="invoices")
public class Invoice extends BaseEntity {

    @OneToOne
    @JoinColumn(name="order_id")
    private Order order;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "invoice")
    private Set<InvoiceLine> invoiceLines = new HashSet<>();

    public void setOrder(Order order) {
        this.order = order;
    }

    public Order getOrder() {
        return order;
    }

    public void setInvoiceLines(Set<InvoiceLine> invoiceLines) {
        this.invoiceLines = invoiceLines;
    }

    public Set<InvoiceLine> getInvoiceLines() {
        return invoiceLines;
    }

    public void addInvoiceLine(InvoiceLine invoiceLine) {
        invoiceLine.setInvoice(this);

        this.invoiceLines.add(invoiceLine);
    }
}
