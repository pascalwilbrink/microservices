package nl.wilbrink.invoices.entity;

import nl.wilbrink.common.entity.BaseEntity;
import nl.wilbrink.common.entity.VatRate;

import javax.persistence.*;

@Entity
@Table(name="invoice_lines")
public class InvoiceLine extends BaseEntity {

    @Column
    private String description;

    @Column
    private Long amount;

    @Column
    private Double priceExVat;

    @Column(name="vat_rate")
    private VatRate vatRate;

    @ManyToOne
    @JoinColumn(name="invoice_id")
    private Invoice invoice;

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public Long getAmount() {
        return amount;
    }

    public void setPriceExVat(Double priceExVat) {
        this.priceExVat = priceExVat;
    }

    public Double getPriceExVat() {
        return priceExVat;
    }

    public void setVatRate(VatRate vatRate) {
        this.vatRate = vatRate;
    }

    public VatRate getVatRate() {
        return vatRate;
    }

    public void setInvoice(Invoice invoice) {
        this.invoice = invoice;
    }

    public Invoice getInvoice() {
        return invoice;
    }

}
