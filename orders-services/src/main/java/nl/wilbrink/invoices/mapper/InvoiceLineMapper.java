package nl.wilbrink.invoices.mapper;

import nl.wilbrink.invoices.dto.InvoiceDTO;
import nl.wilbrink.invoices.entity.Invoice;
import org.mapstruct.Mapper;

@Mapper(componentModel="spring")
public abstract class InvoiceLineMapper {

    public abstract Invoice toEntity(InvoiceDTO dto);

    public abstract InvoiceDTO toDTO(Invoice entity);

}
