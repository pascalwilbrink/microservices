package nl.wilbrink.invoices.mapper;

import nl.wilbrink.invoices.dto.InvoiceDTO;
import nl.wilbrink.invoices.entity.Invoice;
import org.mapstruct.Mapper;

@Mapper(componentModel="spring",uses={InvoiceLineMapper.class})
public abstract class InvoiceMapper {

    public abstract InvoiceDTO toDTO(Invoice entity);

    public abstract Invoice toEntity(InvoiceDTO dto);

}
