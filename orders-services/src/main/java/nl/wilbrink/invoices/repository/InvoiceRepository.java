package nl.wilbrink.invoices.repository;

import nl.wilbrink.invoices.entity.Invoice;
import nl.wilbrink.orders.entity.Order;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InvoiceRepository extends CrudRepository<Invoice,Long> {

    Invoice findOneByOrder(Order oder);

}
