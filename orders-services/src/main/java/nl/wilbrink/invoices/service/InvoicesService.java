package nl.wilbrink.invoices.service;

import nl.wilbrink.common.exception.WebException;
import nl.wilbrink.invoices.dto.InvoiceDTO;
import nl.wilbrink.invoices.entity.Invoice;
import nl.wilbrink.invoices.entity.InvoiceLine;
import nl.wilbrink.invoices.mapper.InvoiceMapper;
import nl.wilbrink.invoices.repository.InvoiceRepository;
import nl.wilbrink.orders.dto.OrderDTO;
import nl.wilbrink.orders.entity.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class InvoicesService {

    private final InvoiceRepository invoiceRepository;

    private final InvoiceMapper invoiceMapper;

    @Autowired
    public InvoicesService(InvoiceRepository invoiceRepository, InvoiceMapper invoiceMapper) {
        this.invoiceRepository = invoiceRepository;
        this.invoiceMapper = invoiceMapper;

        assert(this.invoiceRepository != null);
        assert(this.invoiceMapper != null);
    }

    public Invoice createInvoice(OrderDTO order) {
        Invoice invoice = new Invoice();
        order.getItems().forEach(item -> {
            InvoiceLine invoiceLine = new InvoiceLine();
            invoiceLine.setAmount(item.getAmount());
            invoiceLine.setDescription(item.getBook().getTitle());
            invoiceLine.setPriceExVat(item.getPriceExVat());
            invoiceLine.setVatRate(item.getVatRate());

            invoice.addInvoiceLine(invoiceLine);
        });

        Invoice createdInvoice = invoiceRepository.save(invoice);

        return createdInvoice;
    }

    public InvoiceDTO getInvoice(Long id) {
        Invoice entity = findInvoiceEntity(id);

        return invoiceMapper.toDTO(entity);
    }

    public InvoiceDTO getInvoiceByOrder(Order order) {
        Invoice entity = invoiceRepository.findOneByOrder(order);

        return invoiceMapper.toDTO(entity);
    }

    private Invoice findInvoiceEntity(Long id) {
        Invoice entity = invoiceRepository.findOne(id);

        if(entity == null) {
            throw new WebException(HttpStatus.BAD_REQUEST,"Invoice not found");
        }

        return entity;
    }
}
