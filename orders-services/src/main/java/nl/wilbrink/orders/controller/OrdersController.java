package nl.wilbrink.orders.controller;

import nl.wilbrink.invoices.dto.InvoiceDTO;
import nl.wilbrink.orders.dto.OrderDTO;
import nl.wilbrink.orders.service.OrdersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@RequestMapping("/orders")
public class OrdersController {

    private final OrdersService ordersService;

    @Autowired
    public OrdersController(OrdersService ordersService) {
        this.ordersService = ordersService;

        assert(this.ordersService != null);
    }

    @RequestMapping(method= RequestMethod.POST)
    public ResponseEntity createOrder(@RequestBody OrderDTO order) {
        order = ordersService.createOrder(order);

        return ResponseEntity.ok(order);
    }

    @RequestMapping(value="/{id}")
    public ResponseEntity getOrder(@PathVariable("id") Long id) {
        OrderDTO order = ordersService.getOrder(id);

        return ResponseEntity.ok(order);
    }

    @RequestMapping(value="/{id}/invoice")
    public ResponseEntity getOrderInvoice(@PathVariable("id") Long id) {
        InvoiceDTO invoice = ordersService.getOrderInvoice(id);

        return ResponseEntity.ok(invoice);
    }
}
