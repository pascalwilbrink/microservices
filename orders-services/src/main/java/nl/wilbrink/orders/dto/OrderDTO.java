package nl.wilbrink.orders.dto;

import nl.wilbrink.common.dto.BaseDTO;

import java.io.Serializable;
import java.util.List;

public class OrderDTO extends BaseDTO implements Serializable {

    private String status;

    private List<OrderItemDTO> items;

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setItems(List<OrderItemDTO> items) {
        this.items = items;
    }

    public List<OrderItemDTO> getItems() {
        return items;
    }

}
