package nl.wilbrink.orders.dto;

import nl.wilbrink.books.dto.BookDTO;
import nl.wilbrink.common.dto.BaseDTO;
import nl.wilbrink.common.entity.VatRate;

import java.io.Serializable;

public class OrderItemDTO extends BaseDTO implements Serializable {

    private Long amount;

    private BookDTO book;

    private double PriceExVat;

    private VatRate vatRate;

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public Long getAmount() {
        return amount;
    }

    public void setBook(BookDTO book) {
        this.book = book;
    }

    public BookDTO getBook() {
        return book;
    }

    public void setPriceExVat(double priceExVat) {
        PriceExVat = priceExVat;
    }

    public double getPriceExVat() {
        return PriceExVat;
    }

    public void setVatRate(VatRate vatRate) {
        this.vatRate = vatRate;
    }

    public VatRate getVatRate() {
        return vatRate;
    }

}

