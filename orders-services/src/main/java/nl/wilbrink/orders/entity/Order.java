package nl.wilbrink.orders.entity;

import nl.wilbrink.common.entity.BaseEntity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="orders")
public class Order extends BaseEntity implements Serializable {

    @Column
    private Status status;

    @OneToMany(fetch= FetchType.LAZY,mappedBy="order")
    private Set<OrderItem> orderItems = new HashSet<>();

    public void setStatus(Status status) {
        this.status = status;
    }

    public Status getStatus() {
        return status;
    }

    public void setOrderItems(Set<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }

    public Set<OrderItem> getOrderItems() {
        return orderItems;
    }

    public void addOrderItem(OrderItem orderItem) {
        orderItem.setOrder(this);

        this.orderItems.add(orderItem);
    }

    public enum Status {
        Unpaid,
        Paid,
        WaitingForDelivery,
        Delivering,
        Delivered
    }

}
