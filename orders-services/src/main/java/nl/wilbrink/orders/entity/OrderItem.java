package nl.wilbrink.orders.entity;

import nl.wilbrink.common.entity.BaseEntity;
import nl.wilbrink.common.entity.VatRate;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="order_items")
public class OrderItem extends BaseEntity implements Serializable {

    @ManyToOne
    @JoinColumn(name="order_id")
    private Order order;

    @Column
    private Long amount;

    @Column
    private double priceExVat;

    @Column
    private VatRate vatRate;

    @Column(name="book_id")
    private Long bookId;

    public void setOrder(Order order) {
        this.order = order;
    }

    public Order getOrder() {
        return order;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public Long getAmount() {
        return amount;
    }

    public void setPriceExVat(double priceExVat) {
        this.priceExVat = priceExVat;
    }

    public double getPriceExVat() {
        return priceExVat;
    }

    public void setVatRate(VatRate vatRate) {
        this.vatRate = vatRate;
    }

    public VatRate getVatRate() {
        return vatRate;
    }

    public void setBookId(Long bookId) {
        this.bookId = bookId;
    }

    public Long getBookId() {
        return bookId;
    }
}
