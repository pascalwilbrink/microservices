package nl.wilbrink.orders.mapper;

import nl.wilbrink.books.service.BooksService;
import nl.wilbrink.orders.dto.OrderItemDTO;
import nl.wilbrink.orders.entity.OrderItem;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(componentModel="spring")
public abstract class OrderItemMapper {

    @Autowired
    private BooksService booksService;

    @Mapping(target="book",ignore=true)
    public abstract OrderItemDTO toDTO(OrderItem entity);

    @Mapping(target="bookId",source="book.id")
    public abstract OrderItem toEntity(OrderItemDTO dto);

    @AfterMapping
    public void fillOrderItemBooks(@MappingTarget OrderItemDTO dto, OrderItem entity) {
        dto.setBook(booksService.getBook(entity.getBookId()));
    }

}
