package nl.wilbrink.orders.mapper;

import nl.wilbrink.orders.dto.OrderDTO;
import nl.wilbrink.orders.entity.Order;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper(componentModel="spring", uses={OrderItemMapper.class})
public abstract class OrderMapper {

    public abstract OrderDTO toDTO(Order entity);

    public abstract Order toEntity(OrderDTO dto);

    @Mapping(target="status",ignore=true)
    public abstract void toEntity(@MappingTarget Order entity, OrderDTO dto);

}