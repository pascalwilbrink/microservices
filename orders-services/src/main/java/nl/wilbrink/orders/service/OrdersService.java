package nl.wilbrink.orders.service;

import nl.wilbrink.books.service.BooksService;
import nl.wilbrink.common.exception.WebException;
import nl.wilbrink.invoices.dto.InvoiceDTO;
import nl.wilbrink.invoices.service.InvoicesService;
import nl.wilbrink.orders.dto.OrderDTO;
import nl.wilbrink.orders.entity.Order;
import nl.wilbrink.orders.mapper.OrderMapper;
import nl.wilbrink.orders.repository.OrderRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class OrdersService {

    private final OrderRepository orderRepository;

    private final OrderMapper orderMapper;

    private final InvoicesService invoicesService;

    private final BooksService booksService;

    public OrdersService(OrderRepository orderRepository, OrderMapper orderMapper, InvoicesService invoicesService, BooksService booksService) {
        this.orderRepository = orderRepository;
        this.orderMapper = orderMapper;
        this.invoicesService = invoicesService;
        this.booksService = booksService;

        assert(this.orderRepository != null);
        assert (this.orderMapper != null);
        assert (this.invoicesService != null);
    }

    public OrderDTO getOrder(Long id) {
        Order entity = findOrderEntity(id);

        return orderMapper.toDTO(entity);
    }

    public OrderDTO createOrder(OrderDTO order) {
        order.getItems().forEach(orderItemDTO -> {
            orderItemDTO.setBook(booksService.getBook(orderItemDTO.getBook().getId()));
        });

        Order entity = orderMapper.toEntity(order);

        entity = orderRepository.save(entity);

        invoicesService.createInvoice(orderMapper.toDTO(entity));

        return orderMapper.toDTO(entity);
    }

    public OrderDTO updateOrder(OrderDTO order) {
        Order entity = findOrderEntity(order.getId());

        orderMapper.toEntity(entity,order);

        entity = orderRepository.save(entity);

        return orderMapper.toDTO(entity);
    }

    public InvoiceDTO getOrderInvoice(Long id) {
        Order entity = findOrderEntity(id);

        InvoiceDTO invoice = invoicesService.getInvoiceByOrder(entity);

        return invoice;
    }

    private Order findOrderEntity(Long id) {
        if(id == null) {
            throw new WebException(HttpStatus.BAD_REQUEST,"Order not found");
        }

        Order entity = orderRepository.findOne(id);

        if(entity == null) {
            throw new WebException(HttpStatus.BAD_REQUEST,"Order not found");
        }

        return entity;
    }
}
