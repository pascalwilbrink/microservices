package nl.wilbrink.invoices.mapper;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.annotation.Generated;
import nl.wilbrink.common.entity.VatRate;
import nl.wilbrink.invoices.dto.InvoiceDTO;
import nl.wilbrink.invoices.dto.InvoiceLineDTO;
import nl.wilbrink.invoices.entity.Invoice;
import nl.wilbrink.invoices.entity.InvoiceLine;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2017-09-19T19:47:28+0200",
    comments = "version: 1.2.0.CR2, compiler: javac, environment: Java 1.8.0_144 (Oracle Corporation)"
)
@Component
public class InvoiceLineMapperImpl extends InvoiceLineMapper {

    @Override
    public Invoice toEntity(InvoiceDTO dto) {
        if ( dto == null ) {
            return null;
        }

        Invoice invoice = new Invoice();

        invoice.setId( dto.getId() );
        invoice.setVersion( dto.getVersion() );
        invoice.setCreated( dto.getCreated() );
        invoice.setUpdated( dto.getUpdated() );
        invoice.setCreatedBy( dto.getCreatedBy() );
        invoice.setUpdatedBy( dto.getUpdatedBy() );
        invoice.setInvoiceLines( invoiceLineDTOListToInvoiceLineSet( dto.getInvoiceLines() ) );

        return invoice;
    }

    @Override
    public InvoiceDTO toDTO(Invoice entity) {
        if ( entity == null ) {
            return null;
        }

        InvoiceDTO invoiceDTO = new InvoiceDTO();

        invoiceDTO.setId( entity.getId() );
        invoiceDTO.setVersion( entity.getVersion() );
        invoiceDTO.setCreated( entity.getCreated() );
        invoiceDTO.setUpdated( entity.getUpdated() );
        invoiceDTO.setCreatedBy( entity.getCreatedBy() );
        invoiceDTO.setUpdatedBy( entity.getUpdatedBy() );
        invoiceDTO.setInvoiceLines( invoiceLineSetToInvoiceLineDTOList( entity.getInvoiceLines() ) );

        return invoiceDTO;
    }

    protected InvoiceLine invoiceLineDTOToInvoiceLine(InvoiceLineDTO invoiceLineDTO) {
        if ( invoiceLineDTO == null ) {
            return null;
        }

        InvoiceLine invoiceLine = new InvoiceLine();

        invoiceLine.setId( invoiceLineDTO.getId() );
        invoiceLine.setVersion( invoiceLineDTO.getVersion() );
        invoiceLine.setCreated( invoiceLineDTO.getCreated() );
        invoiceLine.setUpdated( invoiceLineDTO.getUpdated() );
        invoiceLine.setCreatedBy( invoiceLineDTO.getCreatedBy() );
        invoiceLine.setUpdatedBy( invoiceLineDTO.getUpdatedBy() );
        invoiceLine.setDescription( invoiceLineDTO.getDescription() );
        invoiceLine.setAmount( invoiceLineDTO.getAmount() );
        invoiceLine.setPriceExVat( invoiceLineDTO.getPriceExVat() );
        if ( invoiceLineDTO.getVatRate() != null ) {
            invoiceLine.setVatRate( Enum.valueOf( VatRate.class, invoiceLineDTO.getVatRate() ) );
        }

        return invoiceLine;
    }

    protected Set<InvoiceLine> invoiceLineDTOListToInvoiceLineSet(List<InvoiceLineDTO> list) {
        if ( list == null ) {
            return null;
        }

        Set<InvoiceLine> set = new HashSet<InvoiceLine>( Math.max( (int) ( list.size() / .75f ) + 1, 16 ) );
        for ( InvoiceLineDTO invoiceLineDTO : list ) {
            set.add( invoiceLineDTOToInvoiceLine( invoiceLineDTO ) );
        }

        return set;
    }

    protected InvoiceLineDTO invoiceLineToInvoiceLineDTO(InvoiceLine invoiceLine) {
        if ( invoiceLine == null ) {
            return null;
        }

        InvoiceLineDTO invoiceLineDTO = new InvoiceLineDTO();

        invoiceLineDTO.setId( invoiceLine.getId() );
        invoiceLineDTO.setVersion( invoiceLine.getVersion() );
        invoiceLineDTO.setCreated( invoiceLine.getCreated() );
        invoiceLineDTO.setUpdated( invoiceLine.getUpdated() );
        invoiceLineDTO.setCreatedBy( invoiceLine.getCreatedBy() );
        invoiceLineDTO.setUpdatedBy( invoiceLine.getUpdatedBy() );
        invoiceLineDTO.setDescription( invoiceLine.getDescription() );
        invoiceLineDTO.setAmount( invoiceLine.getAmount() );
        invoiceLineDTO.setPriceExVat( invoiceLine.getPriceExVat() );
        if ( invoiceLine.getVatRate() != null ) {
            invoiceLineDTO.setVatRate( invoiceLine.getVatRate().name() );
        }

        return invoiceLineDTO;
    }

    protected List<InvoiceLineDTO> invoiceLineSetToInvoiceLineDTOList(Set<InvoiceLine> set) {
        if ( set == null ) {
            return null;
        }

        List<InvoiceLineDTO> list = new ArrayList<InvoiceLineDTO>( set.size() );
        for ( InvoiceLine invoiceLine : set ) {
            list.add( invoiceLineToInvoiceLineDTO( invoiceLine ) );
        }

        return list;
    }
}
