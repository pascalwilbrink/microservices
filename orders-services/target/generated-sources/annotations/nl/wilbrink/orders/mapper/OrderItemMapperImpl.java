package nl.wilbrink.orders.mapper;

import javax.annotation.Generated;
import nl.wilbrink.books.dto.BookDTO;
import nl.wilbrink.orders.dto.OrderItemDTO;
import nl.wilbrink.orders.entity.OrderItem;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2017-09-19T19:47:28+0200",
    comments = "version: 1.2.0.CR2, compiler: javac, environment: Java 1.8.0_144 (Oracle Corporation)"
)
@Component
public class OrderItemMapperImpl extends OrderItemMapper {

    @Override
    public OrderItemDTO toDTO(OrderItem entity) {
        if ( entity == null ) {
            return null;
        }

        OrderItemDTO orderItemDTO = new OrderItemDTO();

        orderItemDTO.setId( entity.getId() );
        orderItemDTO.setVersion( entity.getVersion() );
        orderItemDTO.setCreated( entity.getCreated() );
        orderItemDTO.setUpdated( entity.getUpdated() );
        orderItemDTO.setCreatedBy( entity.getCreatedBy() );
        orderItemDTO.setUpdatedBy( entity.getUpdatedBy() );
        orderItemDTO.setAmount( entity.getAmount() );
        orderItemDTO.setPriceExVat( entity.getPriceExVat() );
        orderItemDTO.setVatRate( entity.getVatRate() );

        fillOrderItemBooks( orderItemDTO, entity );

        return orderItemDTO;
    }

    @Override
    public OrderItem toEntity(OrderItemDTO dto) {
        if ( dto == null ) {
            return null;
        }

        OrderItem orderItem = new OrderItem();

        Long id = dtoBookId( dto );
        if ( id != null ) {
            orderItem.setBookId( id );
        }
        orderItem.setId( dto.getId() );
        orderItem.setVersion( dto.getVersion() );
        orderItem.setCreated( dto.getCreated() );
        orderItem.setUpdated( dto.getUpdated() );
        orderItem.setCreatedBy( dto.getCreatedBy() );
        orderItem.setUpdatedBy( dto.getUpdatedBy() );
        orderItem.setAmount( dto.getAmount() );
        orderItem.setPriceExVat( dto.getPriceExVat() );
        orderItem.setVatRate( dto.getVatRate() );

        return orderItem;
    }

    private Long dtoBookId(OrderItemDTO orderItemDTO) {
        if ( orderItemDTO == null ) {
            return null;
        }
        BookDTO book = orderItemDTO.getBook();
        if ( book == null ) {
            return null;
        }
        Long id = book.getId();
        if ( id == null ) {
            return null;
        }
        return id;
    }
}
