package nl.wilbrink.orders.mapper;

import javax.annotation.Generated;
import nl.wilbrink.orders.dto.OrderDTO;
import nl.wilbrink.orders.entity.Order;
import nl.wilbrink.orders.entity.Order.Status;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2017-09-19T19:47:28+0200",
    comments = "version: 1.2.0.CR2, compiler: javac, environment: Java 1.8.0_144 (Oracle Corporation)"
)
@Component
public class OrderMapperImpl extends OrderMapper {

    @Override
    public OrderDTO toDTO(Order entity) {
        if ( entity == null ) {
            return null;
        }

        OrderDTO orderDTO = new OrderDTO();

        orderDTO.setId( entity.getId() );
        orderDTO.setVersion( entity.getVersion() );
        orderDTO.setCreated( entity.getCreated() );
        orderDTO.setUpdated( entity.getUpdated() );
        orderDTO.setCreatedBy( entity.getCreatedBy() );
        orderDTO.setUpdatedBy( entity.getUpdatedBy() );
        if ( entity.getStatus() != null ) {
            orderDTO.setStatus( entity.getStatus().name() );
        }

        return orderDTO;
    }

    @Override
    public Order toEntity(OrderDTO dto) {
        if ( dto == null ) {
            return null;
        }

        Order order = new Order();

        order.setId( dto.getId() );
        order.setVersion( dto.getVersion() );
        order.setCreated( dto.getCreated() );
        order.setUpdated( dto.getUpdated() );
        order.setCreatedBy( dto.getCreatedBy() );
        order.setUpdatedBy( dto.getUpdatedBy() );
        if ( dto.getStatus() != null ) {
            order.setStatus( Enum.valueOf( Status.class, dto.getStatus() ) );
        }

        return order;
    }

    @Override
    public void toEntity(Order entity, OrderDTO dto) {
        if ( dto == null ) {
            return;
        }

        entity.setId( dto.getId() );
        entity.setVersion( dto.getVersion() );
        entity.setCreated( dto.getCreated() );
        entity.setUpdated( dto.getUpdated() );
        entity.setCreatedBy( dto.getCreatedBy() );
        entity.setUpdatedBy( dto.getUpdatedBy() );
    }
}
