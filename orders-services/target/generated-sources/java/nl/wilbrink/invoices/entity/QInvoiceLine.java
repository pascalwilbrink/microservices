package nl.wilbrink.invoices.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QInvoiceLine is a Querydsl query type for InvoiceLine
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QInvoiceLine extends EntityPathBase<InvoiceLine> {

    private static final long serialVersionUID = 236609628L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QInvoiceLine invoiceLine = new QInvoiceLine("invoiceLine");

    public final nl.wilbrink.common.entity.QBaseEntity _super = new nl.wilbrink.common.entity.QBaseEntity(this);

    public final NumberPath<Long> amount = createNumber("amount", Long.class);

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    //inherited
    public final NumberPath<Long> createdBy = _super.createdBy;

    public final StringPath description = createString("description");

    //inherited
    public final NumberPath<Long> id = _super.id;

    public final QInvoice invoice;

    public final NumberPath<Double> priceExVat = createNumber("priceExVat", Double.class);

    //inherited
    public final DateTimePath<java.util.Date> updated = _super.updated;

    //inherited
    public final NumberPath<Long> updatedBy = _super.updatedBy;

    public final EnumPath<nl.wilbrink.common.entity.VatRate> vatRate = createEnum("vatRate", nl.wilbrink.common.entity.VatRate.class);

    //inherited
    public final NumberPath<Long> version = _super.version;

    public QInvoiceLine(String variable) {
        this(InvoiceLine.class, forVariable(variable), INITS);
    }

    public QInvoiceLine(Path<? extends InvoiceLine> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QInvoiceLine(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QInvoiceLine(PathMetadata metadata, PathInits inits) {
        this(InvoiceLine.class, metadata, inits);
    }

    public QInvoiceLine(Class<? extends InvoiceLine> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.invoice = inits.isInitialized("invoice") ? new QInvoice(forProperty("invoice"), inits.get("invoice")) : null;
    }

}

